package zip;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.swing.JOptionPane;

public class ZipFile {
	/*
	 * 用于把文件转化为压缩输出流
	 */
	
	public static void main(String[] args) {
//	       String fileDirUrl="C:\\Users\\15358\\Desktop\\Flash";
//	       String dFileUrl="C:\\Users\\15358\\Desktop\\jieya\\";
//			byte[] fileBytes=compressFileDirToBytes(fileDirUrl); //将文件夹数据转压缩zip
//			decomFileDirReadBytes(fileBytes, dFileUrl);
		try {
			byte[] bytes = compressFileToBytes(new File("C:\\Users\\15358\\Desktop\\表白.png"));
			System.out.println(bytes.length);
			decomFileReadBytes(bytes,"C:\\Users\\15358\\Desktop\\ClientDir\\");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	
	//文件压缩输出流
	public static byte[] compressFileToBytes(File file) throws Exception {
		if(!file.exists()) {
			throw new Exception("文件不存在");
		}
		FileInputStream fin =new FileInputStream(file);
		ByteArrayOutputStream bao =new ByteArrayOutputStream();
		ZipOutputStream zos=new ZipOutputStream(bao);
	    byte[] bs=new byte[1024];
	    int c=-1;
	    zos.putNextEntry(new ZipEntry(file.getName()));
	    while((c=fin.read(bs, 0, bs.length))!=-1) {
	    	zos.write(bs, 0, c);
	    }
	    bao.flush();//关闭资源
	    fin.close();
	    zos.close();
	    bao.close();
	    return bao.toByteArray(); //返回字节流
	}
	
	public static void decomFileReadBytes(byte[] bs,String url) {
		try {
			ByteArrayInputStream bin =new ByteArrayInputStream(bs,0,bs.length);
			ZipInputStream zin=new ZipInputStream(bin);
			String fileName=zin.getNextEntry().getName();
			FileOutputStream fos=new FileOutputStream(url+fileName);
			byte[] data=new byte[1024];
			int c=-1;
			while((c=zin.read(data, 0, data.length))!=-1) {
				fos.write(data, 0, c);
			}
			fos.flush();
			fos.close();
			zin.close();
			bin.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	//文件夹压缩输出流
	
	public static void decomFileDirReadBytes(byte[] bs, String fileUrl) { // 读取bytes解压文件夹
		ZipInputStream zin = null;
		ZipEntry entry = null;
		ByteArrayInputStream bin = new ByteArrayInputStream(bs, 0, bs.length); // 读取bytes
		try {
			zin = new ZipInputStream(bin);
			decom(zin, fileUrl);
			//System.out.println("压缩成功!!");
		} finally {
			if (zin != null) {
				try {
					zin.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	private static void decom(ZipInputStream zin, String fileUrl) { //递归解压文件

		ZipEntry entry = null;
		try {
			while ((entry = zin.getNextEntry()) != null) {
				String url = entry.getName();// 获取文件名
				String createFileUrl = fileUrl + url;
				File file = new File(createFileUrl);
				if (url.endsWith(File.separator)) {
					if (!file.exists()) {
						file.mkdirs();// 按路径创建文件夹
						System.out.println("创建文件路径-->" + createFileUrl);
				//		System.out.println("创建文件夹!!");
					}
				} else {
					System.out.println("解压文件-->"+createFileUrl);
					FileOutputStream fos = new FileOutputStream(file);
					byte[] bs = new byte[1024];
					int c = -1;
					while ((c = zin.read(bs, 0, bs.length)) != -1) {
						fos.write(bs, 0, c);
					}
					fos.flush();
					fos.close();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	public static byte[] compressFileDirToBytes(String fileUrl){ //压缩文件夹转为bytes
		File file = new File(fileUrl);
		ByteArrayOutputStream bao=new ByteArrayOutputStream();
		ZipOutputStream out = null;
		try {
			out = new ZipOutputStream(bao);

			if (!file.exists() && !file.isDirectory()) {
				JOptionPane.showConfirmDialog(null, "文件错误不存在或不是文件夹!");
				throw new Exception("文件错误不存在或不是文件夹");
			}
			// 文件存在和是文件夹
			File[] files = file.listFiles();
			out.putNextEntry(new ZipEntry(file.getName()+File.separator));
			if (files.length > 0) {
				for (File f : files) {
					compress(out, file.getName(), f);
				}
			}
			out.flush();
			out.close();
			System.out.println("压缩完成!!");
			return bao.toByteArray();
		} catch (FileNotFoundException e) {
			JOptionPane.showConfirmDialog(null, "压缩失败-->\n" + e.getMessage());
			return null;
		} catch (IOException e) {
			JOptionPane.showConfirmDialog(null, "压缩失败-->\n" + e.getMessage());
			return null;
		} catch (Exception e) {
			JOptionPane.showConfirmDialog(null, "压缩失败-->\n" + e.getMessage());
			return null;
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
	}
	
	private static void compress(ZipOutputStream out, String parentUrl, File file) {

		if (file.isDirectory()) {
			try {
				if (!parentUrl.equals("")) {
					parentUrl += File.separator;
				}
				out.putNextEntry(new ZipEntry(parentUrl + file.getName() + File.separator));
				System.out.println("写入文件夹--》" + file.getName() + File.separator);
				File[] files = file.listFiles();
				if (files != null && files.length > 0) {
					for (File f : files) {
						compress(out, parentUrl + file.getName(), f);
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				if (!parentUrl.equals("")) {
					parentUrl += File.separator;
				}
				out.putNextEntry(new ZipEntry(parentUrl + file.getName()));
				System.out.println("写入文件-->" + parentUrl + file.getName());
				String url = file.getAbsolutePath();
				FileInputStream in = new FileInputStream(url);
				byte[] bs = new byte[1024];
				int c = -1;
				while ((c = in.read(bs, 0, bs.length)) != -1) {
					out.write(bs, 0, c);
				}
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	
}
