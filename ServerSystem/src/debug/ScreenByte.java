package debug;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ScreenByte {

	Robot robot;
	Rectangle rect;

	public ScreenByte() {
		try {
			robot = new Robot();
			rect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private byte[] getScreenBytes() throws IOException {
		BufferedImage img = robot.createScreenCapture(rect);
		ByteArrayOutputStream bao = new ByteArrayOutputStream();
		ImageIO.write(img, "jpg", bao);
		return bao.toByteArray();
	}

	public static void main(String[] args) {
           ScreenByte sb=new ScreenByte();
          try {
        	  for(int i=0;i<3;i++) { 		  
        		  byte[] img= sb.getScreenBytes();
        		  FileOutputStream fos=new FileOutputStream("C:\\Users\\15358\\Desktop\\screen"+i+".jpg");
        		  fos.write(img, 0, img.length);
        		  fos.flush();
        		  fos.close();
        	  }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
